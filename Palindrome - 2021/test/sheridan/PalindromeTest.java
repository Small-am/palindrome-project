package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue("Error", Palindrome.isPalindrome( "anna" ));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Error", Palindrome.isPalindrome( "123" ));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Error", Palindrome.isPalindrome( "tacocat" ));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Error", Palindrome.isPalindrome( "this is not palindrome" ));
	}		
	
}
